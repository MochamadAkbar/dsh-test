@extends('layouts.admin')
@section('css')
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('js')
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        @role('Super Admin')
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info-gradient">
              <div class="inner">
                <h3>{{$usr}}</h3>

                <p>User</p>
              </div>
              <div class="icon">
              <i class="fa fa-user-plus" style="font-size:75px"></i>
              </div>
            <a href="{{route('user.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-dark-gradient">
              <div class="inner">
                <h3>{{$masuk}}</h3>

                <p>Surat Masuk</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope" style="font-size:75px"></i>
              </div>
            <a href="{{route('suratmasuk.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$keluar}}</h3>

                <p>Surat Keluar</p>
              </div>
              <div class="icon">
                <i class="fa fa-paper-plane"style="font-size:75px"></i>
              </div>
              <a href="{{route('suratkeluar.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary-gradient" style="color:whitesmoke">
              <div class="inner">
                <h3>{{$disposisi}}</h3>

                <p>Disposisi</p>
              </div>
              <div class="icon">
                <i class="fa fa-exchange" style="font-size:75px"></i>
              </div>
              <a href="{{route('disposisi.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        @else
        <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info-gradient">
                <div class="inner">
                  <h3>{{$instansi}}</h3>

                  <p>Instansi</p>
                </div>
                <div class="icon">
                <i class="fa fa-briefcase" style="font-size:75px"></i>
                </div>
              <a href="{{route('instansi.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-dark-gradient">
                <div class="inner">
                  <h3>{{$masuk}}</h3>

                  <p>Surat Masuk</p>
                </div>
                <div class="icon">
                  <i class="fa fa-envelope" style="font-size:75px"></i>
                </div>
              <a href="{{route('suratmasuk.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>{{$keluar}}</h3>

                  <p>Surat Keluar</p>
                </div>
                <div class="icon">
                  <i class="fa fa-paper-plane"style="font-size:75px"></i>
                </div>
                <a href="{{route('suratkeluar.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-primary-gradient" style="color:whitesmoke">
                <div class="inner">
                  <h3>{{$disposisi}}</h3>

                  <p>Disposisi</p>
                </div>
                <div class="icon">
                  <i class="fa fa-exchange" style="font-size:75px"></i>
                </div>
                <a href="{{route('disposisi.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          @endrole
        <!-- /.row -->
        <!-- Main row -->
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
