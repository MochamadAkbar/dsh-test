@extends('layouts.app')
@section('css')
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection
@section('content')
<!-- Font Awesome -->
<div class="section__content section__content--p30">
<div class="container-fluid">
    <div class="row m-t-30">
        <div class="col-md-12">
            <div class="m-b-10">
                    <div class="card card-dark" >
            <div class="card-header">
                <h3 class="card-title">Data User
                    <a href= "{{ route('user.create') }}"class="btn btn-outline-success float-right fa fa-pencil-square-o">
                    <i class="fa fa-pencil-square-o; "></i>
                    Tambah Data
                    </a>
            </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <table id="example1" class="table table-borderless table-data3"style="font-size:11pt">
                <thead>
                <tr>
                    <th>No #</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                        @php
                          $no = 1;
                        @endphp
                        @foreach ($usr as $data)
                        <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ $data->user_name }}</td>
                          <td>{{ $data->nama_user }}</td>
                          <td>
                            <a href="{{ route('user.edit', $data->id) }}" class="btn btn-outline-primary">Edit</a>
                          </td>
                          <td>
                            <form action="{{ route('user.destroy', $data->id) }}" method="post">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="_method" value="DELETE">
                              <button type="submit" class="btn btn-outline-danger">Delete</button>
                            </form>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
            </table>
        </div>
    </div>
</div>
</div>
      </div>
  </div>
</div>
@include('User.create')
@endsection
