@extends('layouts.app')
@section('content')
<div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-t-30">
                <div class="col-md-12">
                    <div class="m-b-10">
                            <div class="card card-dark" >
                            <div class="card-header">
                                    <h3 class="card-title">Add Data User
                                            <div class="panel-title pull-right">
												<i class="fa fa-mail-reply btn btn-outline-info"><a href="{{ url('/home')}}"> Kembali</a></i>
                                            </div>
                                        </h3>
                                    </div>
							<div class="card-body">
								<form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
										<label class="control-label">Username</label>
										<input type="text" name="user_name" class="form-control"  required>
										@if ($errors->has('user_name'))
											<span class="help-block">
												<strong>{{ $errors->first('user_name') }}</strong>
											</span>
										@endif
										</div>
										<div class="form-group {{ $errors->has('nama_user') ? 'has-error' : '' }}">
											<label class="control-label">Nama</label>
											<input type="text" name="nama_user" class="form-control"  required>
											@if ($errors->has('nama_user'))
												<span class="help-block">
													<strong>{{ $errors->first('nama_user') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
											<label class="control-label">Password</label>
											<input type="password" name="password" class="form-control"  required>
											@if ($errors->has('password'))
												<span class="help-block">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
											@endif
											</div>
											<div class="form-group ">
												<label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
													<input id="password-confirm" type="password" class="form-control" name="password_confirmation"  required>
											</div>
									<div>
                                    <button type="submit" class="btn btn-outline-primary">
                                            <i class="fa fa-check"></i>
                                            Add
                                        </button>
							</div>
						</form>
				</div>
			</div>
		</div>
    </div>
</div>
</div>
@endsection
