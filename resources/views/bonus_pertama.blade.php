@extends('layouts.app')
@section('content')
<?php

$i;
for ($i = 1; $i <= 24; $i++)
{
    if ($i % 2 == 0 && $i % 3 == 0)
        echo "CodilityTest" . '<br>';

    else if ($i % 2 == 0 && $i % 5 == 0)
        echo "CodilityCoders" . '<br>';

    else if ($i % 3 == 0 && $i % 5 == 0)
        echo "TestCoders" . '<br>';

    else if (($i % 2) == 0)
        echo "Codility" . '<br>';

    else if (($i % 3) == 0)
        echo "Test" . '<br>';

    else if (($i % 5) == 0)
        echo "Coders" . '<br>';

    else
        echo $i,"  " ;
}

?>
@endsection