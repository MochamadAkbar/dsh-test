<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = new Role;
        $adminRole->name = "Super Admin";
        $adminRole->display_name = "Super Admin";
        $adminRole->save();

        $memberRole = new Role;
        $memberRole->name ="Admin";
        $memberRole->display_name = "Admin";
        $memberRole->save();

        $admin = new User;
        $admin->nama_user= "Mochamad Akbar";
        $admin->user_name = "Super Admin";
        $admin->jabatan = "Manager";
        $admin->password = bcrypt('admin123');
        $admin->save();
        $admin->attachRole($adminRole);

        $member = new User;
        $member->nama_user = "Admin";
        $member->user_name = "Admin";
        $member->jabatan = "Sekertaris Manager";
        $member->password = bcrypt('member123');
        $member->save();
        $member->attachRole($memberRole);
    }
}
