<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust\LaratrustFacade as Laratrust;
use App\Surat_masuk;
use App\Surat_keluar;
use App\Disposisi;
use App\Instansi;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usr = User::count();
        $masuk = Surat_masuk::count();
        $keluar = Surat_keluar::count();
        $disposisi = Disposisi::count();
        $instansi = Instansi::count();

        return view('home',compact('usr', 'masuk', 'keluar', 'disposisi', 'instansi'));
        // if(Laratrust::hasRole('Super Admin')) return $this->adminDashboard();
        // if(Laratrust::hasRole('Admin')) return $this->memberDashboard();
    }
        // protected function adminDashboard()
        // {
        //     return view('home');
        // }
        // protected function adminDashboard()
        // {
        //     return view('welcome');
        // }
}
