<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usr= User::with('role')->get();
        return view('User.indexuser', compact('usr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('User.creater');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_name'=>'required|max:255|unique:users',
            'nama_user'=>'required|max:255',
            'password'=> 'required|min:6|confirmed',
        ]);
        $usr = new User;
        $usr->user_name = $request->user_name;
        $usr->nama_user = $request->nama_user;
        $usr->password = bcrypt('$request->password');
        $usr->save();
        $memberRole = Role::where('name', 'Admin')->first();
        $usr->attachRole($memberRole);
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('User.edit', compact('usr'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usr = User::findOrfail($id);
        return view('User.edit', compact('usr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_name'=>'required|max:255',
            'nama_user'=>'required|max:255',
            'password'=> 'required|min:6|confirmed',

        ]);
        $usr = User::findOrFail($id);
        $usr->user_name = $request->user_name;
        $usr->nama_user = $request->nama_user;
        $usr->password = bcrypt($request->password);
        $usr->save();
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usr = User::FIndOrFail($id);
        $usr->delete();
        return redirect()->route('user.index');
    }
}
